package com.example.mmaacc.blackvelvetcinema;

import android.content.Intent;
import android.media.Image;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.LinearLayout;

public class Divergent extends AppCompatActivity {

    private LinearLayout lin1, lin2;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_divergent);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);


         lin1= (LinearLayout) findViewById(R.id.linearLayout1);
        lin2= (LinearLayout) findViewById(R.id.linearLayout2);



        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }
     // when the button Movie Info is pressed
    public void makevisible1(View v){
        lin1.setVisibility(View.VISIBLE);
        lin2.setVisibility(View.INVISIBLE);
    }
    // when the button Display Time is pressed
    public void makevisible2(View v){
        lin1.setVisibility(View.INVISIBLE);
        lin2.setVisibility(View.VISIBLE);
    }


    public void makevisible3(View v){
        startActivity(new Intent(Divergent.this, Reservation.class));
    }
}
