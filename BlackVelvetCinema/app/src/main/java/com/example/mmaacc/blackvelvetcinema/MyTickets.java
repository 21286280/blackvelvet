package com.example.mmaacc.blackvelvetcinema;

import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

public class MyTickets extends AppCompatActivity {

    public LinearLayout linearLayout;
    public TextView noT;
    public int id;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my_tickets);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        noT = (TextView)findViewById(R.id.noT);
        linearLayout= (LinearLayout)findViewById(R.id.layout);
        // create a ticket if the client press OK on ReservationDetails

        if(ReservationDetails.count!=0)
        createOrder();

    }

    public void createOrder(){
        createTextView("");
        createTextView(" Movie: "+ Reservation.movieT);
        createTextView(" Day: "+ Reservation.movieD);
        createTextView(" Time: "+ Reservation.movieH);
        createTextView(" Tickets number: " +Integer.toString(ReservationDetails.ticketsNo));
        createTextView(" Name: " +ReservationDetails.clientN);
    }



    public void createTextView(String text){
        if(id==0){
            noT.setVisibility(View.INVISIBLE);
        }
        id++;

        TextView tx = new TextView(this);
        tx.setText(text);
        tx.setTextSize(20);
        linearLayout.addView(tx);
    }



}
