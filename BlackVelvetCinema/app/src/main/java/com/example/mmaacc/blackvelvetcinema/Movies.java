package com.example.mmaacc.blackvelvetcinema;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.Toast;

public class Movies extends AppCompatActivity {
    ListView list;
    String[] movie={
            "Divergent",
            "Edge of Tomorrow",
            "Ex_machina",
            "Inception",
            "Lucky Number",
            "Predestination",
            "Shutter Island",
            "Spirit Stallion of the Cimarron",
            "The Machinist",
            "Spirited Away"

    };

    Integer[] imageId = {
            R.drawable.divergent,
            R.drawable.edge_of_tomorrow,
            R.drawable.ex_machina,
            R.drawable.inception,
            R.drawable.lucky_number,
            R.drawable.predestination,
            R.drawable.shutter_island,
            R.drawable.spirit_stallion_of_the_cimarron,
            R.drawable.the_machinist,
            R.drawable.spirited_away


    };

    String[] description = {
            "HD",
            "HD,3D",
            "HD",
            "HD",
            "HD",
            "HD",
            "3D",
            "HD,3D",
            "HD",
            "3D"

    };


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_movies);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        MoviesList adapter=new MoviesList(Movies.this,movie,description,imageId);
        list=(ListView)findViewById(R.id.list);
        list.setAdapter(adapter);
        list.setOnItemClickListener(new AdapterView.OnItemClickListener() {

            @Override

        public void  onItemClick(AdapterView<?> parent, View view, int position, long id)
            {
                //Toast.makeText(Movies.this,"You clicked at:" +movie[+ position],Toast.LENGTH_SHORT);
                if(position==0){

                    startActivity(new Intent(Movies.this, Divergent.class));


                }
                if(position==1)
                {
                    startActivity(new Intent(Movies.this, EdgeOfTomorrow.class));
                }
                if(position == 2)
                {
                    startActivity(new Intent(Movies.this, Ex_machina.class));
                }
                if(position == 3)
                {
                    startActivity(new Intent(Movies.this, Inception.class));
                }
                if(position == 4)
                {
                    startActivity(new Intent(Movies.this, LuckyNumber.class));
                }
                if(position == 5)
                {
                    startActivity(new Intent(Movies.this, Predestination.class));
                }

                if(position == 6)
                {
                    startActivity(new Intent(Movies.this, ShutterIsland.class));
                }
                if(position == 7)
                {
                    startActivity(new Intent(Movies.this, SpiritStallion.class));
                }
                if(position == 8)
                {
                    startActivity(new Intent(Movies.this, TheMachinist.class));
                }
                if(position == 9)
                {
                    startActivity(new Intent(Movies.this, SpiritedAway.class));
                }
            }

        });


    }

}
