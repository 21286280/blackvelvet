package com.example.mmaacc.blackvelvetcinema;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    /* Each of these methods open another activity when they
       are called by their respective ImageButton  */

    public void schedule(View v){
        startActivity(new Intent(MainActivity.this, Schedule.class));
    }

    public void movies(View v){
        startActivity(new Intent(MainActivity.this, Movies.class));
    }

    public void reservation(View v){
        startActivity(new Intent(MainActivity.this, Reservation.class));
    }

    public void about(View v){
        startActivity(new Intent(MainActivity.this, About.class));
    }

    public void shop(View v){
        startActivity(new Intent(MainActivity.this, Shop.class));
    }

    public void tickets(View v){
        startActivity(new Intent(MainActivity.this, MyTickets.class));
    }


}
