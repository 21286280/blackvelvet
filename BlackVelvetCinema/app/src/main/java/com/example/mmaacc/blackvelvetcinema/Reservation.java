package com.example.mmaacc.blackvelvetcinema;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.RadioGroup;
import android.widget.ScrollView;
import android.widget.Spinner;
import android.widget.Toast;

import com.google.android.gms.appindexing.Action;
import com.google.android.gms.appindexing.AppIndex;
import com.google.android.gms.common.api.GoogleApiClient;

import java.util.ArrayList;
import java.util.List;

public class Reservation extends AppCompatActivity implements AdapterView.OnItemSelectedListener {

   public static String movieH="None", movieT="None", movieD="None";
    public Button reserve;





    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_reservation);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        reserve = (Button)findViewById(R.id.reserve);

        // Spinner element
        Spinner spinner1 = (Spinner) findViewById(R.id.spinner1);
        Spinner spinner2 = (Spinner) findViewById(R.id.spinner2);
        Spinner spinner3 = (Spinner) findViewById(R.id.spinner3);

        // Spinner click listener
        spinner1.setOnItemSelectedListener(this);
        spinner2.setOnItemSelectedListener(this);
        spinner3.setOnItemSelectedListener(this);


        // Spinner1 Drop down elements
        List<String> movies = new ArrayList<String>();
        movies.add("None");
        movies.add("Divergent");
        movies.add("Edge Of Tomorrow ");
        movies.add("Ex-machina");
        movies.add("Inception");
        movies.add("Lucky Number");
        movies.add("Predestination");
        movies.add("Shutter Island");
        movies.add("Spirit Stalion Of Cimarron");
        movies.add("The Machinist");
        movies.add("Spirited Away");

        // Spinner2 Drop down elements
        List<String> days = new ArrayList<String>();
        days.add("None");
        days.add("Monday");
        days.add("Tuesday");
        days.add("Wednesday");
        days.add("Thursday");
        days.add("Friday");
        days.add("Saturday");
        days.add("Sunday");

        //Spinner3 Drop down elements
        List<String> hours = new ArrayList<String>();
        hours.add("None");
        hours.add("14.00");
        hours.add("16.30");
        hours.add("21.00");




        // Creating adapter for spinner
        ArrayAdapter<String> dataAdapter1 = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_single_choice, movies);
        ArrayAdapter<String> dataAdapter2 = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_single_choice, days);
        ArrayAdapter<String> dataAdapter3 = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_single_choice, hours);


        // Drop down layout style - list view with check button
        dataAdapter1.setDropDownViewResource(android.R.layout.simple_list_item_checked);

        //Drop down layout style - list view with simple select  button
        dataAdapter2.setDropDownViewResource(android.R.layout.simple_list_item_checked);

        //Drop down layout style - list view with single choice  button
        dataAdapter3.setDropDownViewResource(android.R.layout.simple_list_item_single_choice);



        // attaching data adapter to spinner
        spinner1.setAdapter(dataAdapter1);
        spinner2.setAdapter(dataAdapter2);
        spinner3.setAdapter(dataAdapter3);





    }

        @Override
        public void onItemSelected (AdapterView< ? > parent, View view,int position, long id){
            // On selecting a spinner item
            String item = parent.getItemAtPosition(position).toString();


            // Showing selected spinner item
            Toast.makeText(parent.getContext(), "Selected: " + item, Toast.LENGTH_LONG).show();

            Spinner spinner =(Spinner) parent;
            if(spinner.getId()== R.id.spinner1){
                movieT=item;
            }

            if(spinner.getId()==R.id.spinner2){
                movieD=item;
            }

            if(spinner.getId() ==R.id.spinner3){
                movieH=item;
            }
            if(movieH.equals("None")  || movieT.equals("None") || movieD.equals("None")) {
                reserve.setVisibility(View.INVISIBLE);
            }else{
                reserve.setVisibility(View.VISIBLE);
            }
        }




    public void onNothingSelected(AdapterView<?> arg0) {
        // TODO Auto-generated method stub
    }
    public void reserve(View view){
    startActivity(new Intent(Reservation.this, ReservationDetails.class));
}








}


