package com.example.mmaacc.blackvelvetcinema;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

public class ReservationDetails extends AppCompatActivity implements AdapterView.OnItemSelectedListener {



    public static String  clientN;
    public EditText name;
    public static int ticketsNo=1;
    public static int count;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_reservation_details);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        name = (EditText)findViewById(R.id.name);

        // Spinner element
        Spinner spinner1 = (Spinner) findViewById(R.id.spinner1);


        // Spinner click listener
        spinner1.setOnItemSelectedListener(this);


        // Spinner1 Drop down elements
        List<String> quantity = new ArrayList<String>();
        quantity.add("1 ticket");
        quantity.add("2 tickets");
        quantity.add("3 tickets");
        quantity.add("4 tickets");
        quantity.add("5 tickets");
        quantity.add("6 tickets");
        quantity.add("7 tickets");
        quantity.add("8 tickets");
        quantity.add("9 tickets");
        quantity.add("10 tickets");


        // Creating adapter for spinner
        ArrayAdapter<String> dataAdapter1 = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_activated_1, quantity);


        // Drop down layout style - list view with check button
        dataAdapter1.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        // attaching data adapter to spinner
        spinner1.setAdapter(dataAdapter1);

    }

    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
        ticketsNo=position+1;
        // Showing selected spinner item
        Toast.makeText(parent.getContext(), "Selected: " + ticketsNo, Toast.LENGTH_LONG).show();


    }

    public void onNothingSelected(AdapterView<?> arg0) {
        // TODO Auto-generated method stub
    }

    public void ok(View view) {
        clientN=name.getText().toString();
        if(name.getText().toString().equals("")){
            Toast.makeText(ReservationDetails.this, "Please add your name", Toast.LENGTH_SHORT).show();
        }else {
            startActivity(new Intent(ReservationDetails.this, MyTickets.class));
            count++;
        }
        }



}

