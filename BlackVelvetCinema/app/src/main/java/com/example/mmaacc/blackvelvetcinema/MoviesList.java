package com.example.mmaacc.blackvelvetcinema;

/**
 * Created by mmaacc on 11/12/15.
 */

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

public class MoviesList  extends ArrayAdapter<String> {

    private final Activity context;
    private final String[] movie;
    private final Integer[] imageId;
    private final String[] description;

    //this constructor of class is used to create a custom ListView with all what I want : image of movie , title of movie  and description

    public MoviesList(Activity context, String[] movie,String[] description, Integer[] imageId) {
        super(context, R.layout.list_movies, movie);
        this.context = context;
        this.movie = movie;
        this.imageId = imageId;
        this.description=description;

    }

    public View getView(int position, View view, ViewGroup parent) {

        LayoutInflater inflater = context.getLayoutInflater();

        //the following line is made for one row of the ListView
        View rowView = inflater.inflate(R.layout.list_movies, null, true);

        TextView txtTitle = (TextView) rowView.findViewById(R.id.txt);
        TextView txtDescription=(TextView) rowView.findViewById(R.id.txt2);
        ImageView imageView = (ImageView) rowView.findViewById(R.id.img);

        txtTitle.setText(movie[position]);
        txtDescription.setText(description[position]);
        imageView.setImageResource(imageId[position]);


        return rowView;

    }
}
